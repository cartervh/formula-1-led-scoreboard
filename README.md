# Formula 1 Signboard
This project uses the Adafruit Matrix Portal to run a signboard denoting information about the next Formula 1 race.

## What is it?
This project uses the Adafruit Matrix Portal to create a signboard denoting the next race, its location, and how long until lights out.

This scoreboard is lighter-weight, easier to set up, and simpler to maintain than the original [On-Air Pi Project](https://gitlab.com/cartervh/air-rasberry-pi)'s screensaver functionality.

## Hardware
### Required Components:
- [Adafruit Matrix Portal](https://www.adafruit.com/product/4745)
- [64x32 RGB LED Matrix from Adafruit](https://www.adafruit.com/product/2278)
- [USB-C Power Supply](https://www.adafruit.com/product/4298)
- USB-C Cable (Be sure it carries data and not just power)

### Recommended Components:
- [LED Screen Diffuser](https://www.adafruit.com/product/4749)
- [Tablet Stand](https://www.adafruit.com/product/1679)
- [Adhesive Strips (For sticking diffuser to the screen)](https://www.amazon.com/ProTapes-306UGLU600-UGlu-Dash-Sheets/dp/B06XCCRPRY/ref=pd_lpo_2?pd_rd_i=B06XCCRPRY&psc=1)

## Software
- CircuitPython (follow Adafruit guide linked below for installation)

## Setup Guide
### Set up the Matrix Portal
To set up this project, first be sure to follow the [Adafruit MatrixPortal Setup Guide](https://learn.adafruit.com/adafruit-matrixportal-m4) until you are prompted to start writing code. I recommend using Visual Studio Code for writing CircuitPython, if you see the need to modify my code, but do so only at your own risk. 

### Install the project
When prompted to write code into the `code.py` file on the MatrixPortal drive, you can simply copy all of the files in this repository onto it, aside from the lib directory. You can use the lib directory in this repository as a reference point for the lib directory you put on the drive, but the libraries in this repository could be out of date. It's best just to use Adafruit's libraries that are provided in place of the ones here.

You will also need to set up a `secrets.py` file. I have provided a sample one in this repository. You are required to set up an Adafruit.io account as well ([See this page in the MatrixPortal tutorial for how to get your key](https://learn.adafruit.com/adafruit-matrixportal-m4/internet-connect)).

### Prepare for use
At this point you should be good to go software-wise. Now you can follow Adafruit's instructions on [installing the matrix diffuser](https://learn.adafruit.com/adafruit-matrixportal-m4/led-matrix-diffuser), if you got one, set it up on the stand, and plug it in! It will take a moment to boot up, but it will eventually tell you the time and place of the next F1 race! Note, I have found that if you reboot the signboard, sometimes it gives you an out of memory error. Simply reboot again and it should be fine.

## Formula 1 API
I have utilized the [Ergast Developer API](http://ergast.com/mrd/) to retrieve F1 data.
