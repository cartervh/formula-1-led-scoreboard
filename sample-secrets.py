# This file is where you keep secret settings, passwords, and tokens!
# If you put them in the code you risk committing that info or sharing it

# When using, rename this file to secrets.py

secrets = {
    'ssid' : 'Your Wifi Name',
    'password' : 'YourWifiPassword',
    'timezone' : "America/New_York", # http://worldtimeapi.org/timezones
    'aio_username' : 'YourAdafruitIOUsername',
	'aio_key' : 'YourAdafruitIOKey'
    }
