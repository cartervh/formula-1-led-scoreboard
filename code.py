import gc
import displayio
import board
import time
import adafruit_display_text.label
from adafruit_matrixportal.matrix import Matrix
from adafruit_bitmap_font import bitmap_font
from adafruit_matrixportal.network import Network
from adafruit_datetime import datetime, timedelta

#https://learn.adafruit.com/circuitpython-display-support-using-displayio/multiple-tilegrids
#https://github.com/adafruit/Adafruit_CircuitPython_PortalBase/blob/main/adafruit_portalbase
# --- Display setup ---
matrix = Matrix()
display = matrix.display

SCROLL_DELAY = 0.06
START_POS = 64

sessionNames = [{"id":"FirstPractice", "name":"Practice 1"}, 
                {"id":"SecondPractice", "name":"Practice 2"}, 
                {"id":"ThirdPractice","name":"Practice 3"}, 
                {"id":"Qualifying","name":"Qualifying"}, 
                {"id":"Sprint","name":"Sprint"}]
network = Network(status_neopixel=board.NEOPIXEL, debug=False)

color = displayio.Palette(5)
color[0] = 0x000000 #Black
color[1] = 0xFFFFFF #White
color[2] = 0x00FF00 #Green
color[3] = 0xFFDD00 #Yellow
color[4] = 0xFF0000 #Red

def rjustnum(num, desiredLen):
    string =str(num)
    char = '0'
    while(len(string) < desiredLen):
        string = char + '' + string
    return string

# --- Time Setup ---
local_offset = None
network.get_local_time()
localtime =datetime.now()
print('Local Time:', localtime)
network.get_local_time("Etc/GMT")
gmt = datetime.now()
print('GMT Time:', gmt)
offset = localtime - gmt
day_offset = offset.days
hour_offset = 1 + (offset.seconds % 86400) // 3600
print('Timezone Offset:', day_offset, 'days', hour_offset, 'hours')
local_offset = timedelta(days=day_offset, hours=hour_offset)

font = bitmap_font.load_font("/fonts/5x8.bdf")


class F1NextRaceScreen():
    def __init__(self, matrix: Matrix, display):
        print('Initialize F1 Next Race Screen')
        self.matrix = matrix
        self.display = display
        self.bitmap = displayio.Bitmap(64, 32, 1) #Width, Height, Bit Depth (?)
        self.build_group()
        self.display.show(self.group)
        self.race = None
        self.lastRefresh = None
        self.nextSessionInd = 0
        self.sessionIterInd = 0
        self.restart()

    def build_group(self):
        self.group = displayio.Group()
        # Create a TileGrid using the Bitmap and Palette
        tile_grid = displayio.TileGrid(self.bitmap, pixel_shader=color)
            
        # Add the TileGrid to the Group
        self.group.append(tile_grid)
        self.race_name = adafruit_display_text.label.Label(font, color=color[1], x=START_POS, y=6)
        self.session_name = adafruit_display_text.label.Label(font, color=color[1], x=5, y=15)
        self.time_until = adafruit_display_text.label.Label(font, color=color[1], x=5, y=26)
        self.group.append(self.race_name)
        self.group.append(self.session_name)
        self.group.append(self.time_until)
    
    def restart(self):
        print("Restart Mem:", gc.mem_free())
        self.pos = START_POS
        self.update_race()
        
        self.nextSessionInd = self.race.getNextSession()
        self.sessionIterInd = (self.sessionIterInd + 1) % len(self.race.sessionList)
        if self.sessionIterInd < self.nextSessionInd:
            self.sessionIterInd = self.nextSessionInd

        self.race_name.text = self.race.display
        sessionName = self.race.sessionList[self.sessionIterInd].get('name', 'Unknown Session')
        self.session_name.text = sessionName
        self.session_name.x = 32 - int((len(sessionName)*5)/2)
        self.update_datetime()

    def update_race(self):
        hourAgo = datetime.now() + timedelta(hours = -1)
        if (not self.race or not self.lastRefresh or self.lastRefresh < hourAgo):
            print('Refreshing data. Last refresh was', self.lastRefresh)
            self.lastRefresh = datetime.now()
            self.race = Race(F1API.getNextRace())

    def update_datetime(self):
        sessionDT = self.race.sessionList[self.sessionIterInd].get('datetime')
        localSessionDT = sessionDT + local_offset
        self.time_until.text = '{}/{} {}:{}'.format(rjustnum(localSessionDT.month, 2), rjustnum(localSessionDT.day, 2), rjustnum(localSessionDT.hour, 2), rjustnum(localSessionDT.minute, 2))
        diff = sessionDT - datetime.now()
        self.time_until.color = F1NextRaceScreen.get_color_from_days_until(diff.days)

    def get_color_from_days_until(days_until):
        if days_until > 7:
            return color[1]
        elif days_until > 1:
            return color[2]
        elif days_until > 0:
            return color[3]
        else:
            return color[4]

    def update_time_to_timer(self):
        daysUntil = 0
        hoursUntil = 0
        minutesUntil = 0
        secondsUntil = 0
        sessionDT = self.race.sessionList[self.sessionIterInd].get('datetime')
        if (sessionDT and sessionDT > datetime.now()):
            diff = sessionDT - datetime.now()
            daysUntil = diff.days
            hoursUntil = (diff.seconds % 86400) // 3600
            minutesUntil = (diff.seconds % 3600) // 60
            secondsUntil = diff.seconds % 60
        self.time_until.text = rjustnum(daysUntil,2) + ':' + rjustnum(hoursUntil, 2) + ':' + rjustnum(minutesUntil,2) + ':' + rjustnum(secondsUntil,2)

    def iterate(self):
        self.pos -= 1
        self.race_name.x = self.pos
        # self.update_time_to_timer()
        self.display.show(self.group)
        time.sleep(SCROLL_DELAY)
        return self.isCycleDone()

    def isCycleDone(self):
        if self.pos < -(len((self.race_name.text))*5 + 3):
            return True 

class Race():
    def __init__(self, json):
        self.sessionList = []
        self.display = Race.getRaceDisplay(json)
        sessionNumber = 0
        for sessionIdDict in sessionNames:
            sessionId = sessionIdDict.get("id")
            session = json.get(sessionId)
            if session:
                dt = Race.getRaceDateTime(session)
                session['name'] = sessionIdDict.get("name", '')
                session['datetime'] = dt
                self.sessionList.insert(sessionNumber, session)
                sessionNumber += 1
        
        self.raceDT = Race.getRaceDateTime(json)
        raceSession = {'name': 'Race', 'datetime':self.raceDT}
        self.sessionList.insert(len(self.sessionList), raceSession)
    
    def getRaceDisplay(race):
        circuitName = race.get('Circuit', {}).get('circuitName', '')
        raceName = race.get('raceName', '')
        if raceName and circuitName:
            return raceName + ' - ' + circuitName
        elif raceName:
            return raceName
        else:
            return ''

    def getRaceDateTime(race):
        if race:
            time_str = race.get('time', ' ')
            time_str = time_str[0:len(time_str)-1]
            raceDateTime = race.get('date', '') + '*' + time_str #Race date and time is always passed GMT
            dt = datetime.fromisoformat(raceDateTime)
        else:
            dt = None
        return dt

    def getNextSession(self):
        for ind in range(0, len(self.sessionList)):
            session = self.sessionList[ind]
            if (session['datetime'] + timedelta(hours = 1) >= datetime.now()):
                return ind
        return len(self.sessionList) - 1
        
    
class F1API():

    def getNextRace():
        nextRaces = F1API.getRemoteErgastData("http://ergast.com/api/f1/current/next.json").get('RaceTable')
        if len(nextRaces['Races']) > 0:
            return nextRaces['Races'][0]
        else:
            return None

    def getRemoteErgastData(url):
        data_location = ["MRData"]
        response = network.fetch_data((url), json_path=(data_location,))
        print("Getting from URL:", url)
        return response


print("Starting Mem:", gc.mem_free())
screen = F1NextRaceScreen(matrix, display)
print("Post Screen Init Mem:", gc.mem_free())
while True:
    done = screen.iterate()
    if done:
        screen.restart()